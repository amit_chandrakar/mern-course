import Events from 'node:events';

const myEmitter = new Events();

myEmitter.on('sum', (a, b) => {
  console.log(`The sum of ${a} and ${b} is ${a + b}`);
});

myEmitter.emit('sum', 1, 2);