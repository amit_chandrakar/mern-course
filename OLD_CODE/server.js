import http from 'node:http';

const server = http.createServer((request, response) => {
    response.setHeader('Content-Type', 'application/json');

    if (request.url === '/') {
        response.end(JSON.stringify({
            message: 'This is home page'
        }));
    }
    else if (request.url === '/user') {
        response.end(JSON.stringify({
            name: 'John',
            age: 30
        }));
    }
    else if (request.url === '/users/store' && request.method === 'POST') {
        response.end(JSON.stringify({
            message: 'User created'
        }));
    }
    else {
        response.end(JSON.stringify({
            message: 'Route not found'
        }));
    }
});

server.listen(3001, () => {
    console.log('Server is listening on port 3001');
});