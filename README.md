Date - 14 Oct 2023
  - Necessary packages
    - Body-parser
    - Mongoose
    - Cors
  - Use MongoDB in Express.js
  - Use models in Express.js
  - Use schema in Express.js
  - Use middleware in Express.js

Date - 12 Oct 2023
  - Node js remailing topics (FileSystem and URL module)
  - Introduction to Express js
  - Necessary packages
    - Nodemon
    - Express
    - Body-parser
    - Mongoose
    - Cors

Date - 11 Oct 2023
  - ES6 JS Async
    - Callbacks
    - Asynchronous JavaScript
    - Promises
    - Async/Await
  - Node Js
    - Introduction to Node
    - Node Modules
    - Node File System
    - Node Events
    - Node URL Module
    - Node HTTP Server
    - Postman (Testing endpoints)

Date - 10 Oct 2023
  - Introduction to MongoDB
    - What is MongoDB?
    - Why MongoDB?
    - Advantages of MongoDB
    - CRUD from MongoDB Compass
    - Creating a database
    - Creating a collection
    - Inserting documents
    - Querying documents
    - Updating documents
    - Deleting documents
    - Dropping a collection
    - Dropping a database
    - Linking/Joining collections
  - Assignment
    - MongoDB Query Operators

Date - 09 Oct 2023
  - Prerequisites
  - How much JS is required for this course?
  - Introduction to the course (including the syllabus overview)
  - My journey in the MERN stack
  - My expectations from you
  - Your expectations from me
  - How to get the most out of this course?
  - How to ask questions?
  - Necessary Js and ES6 for MERN
    - History and Evolution
    - var, let and const
    - for...of
    - map()
    - Default function parameters
    - Template Literals
    - Arrow Function
    - Modules
    - Import and Export
    - Destructuring
    - Rest parameter and Spread operator
    - Promises
