import { Router } from 'express';
import {index, store, show, update, destroy} from '../controllers/user-controller.js';

const router = Router();

router.get('/', index);
router.post('/store', store);
router.get('/show', show);
router.put('/update', update);
router.delete('/delete', destroy);

export default router;