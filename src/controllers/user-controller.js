import User from '../models/user-model.js';

export const index = async (req, res, next) => {
    var users;

    try {
        users = await User.find();
    } catch (error) {
        return res.json({ status: 'failed', message: 'Unexpected error occured. Please try again later' });
    }

    res.json({ status: 'success', message: 'User created successfully', data: { users : users } });
}

export const store = async (req, res, next) => {
    const {name, email, active} = req.body;
    const user = new User({ name, email, active });

    try {
        await user.save();
    } catch (error) {
        return res.json({ status: 'failed', message: 'Unexpected error occured. Please try again later' });
    }

    res.json({ status: 'success', message: 'User created successfully', data: { user : user } });

    // try {
    //     const user = await User.create({
    //         name: 'John Doe',
    //         email: 'john@example.com',
    //         active: 'true',
    //     });

    //     res.send('User created successfully');
    // } catch (error) {
    //     console.log(error);
    //     res.send(error);
    // }
}

export const show = (req, res, next) => {
    res.send('This is show page');
}

export const update = (req, res, next) => {
    res.send('This is update page');
}

export const destroy = (req, res, next) => {
    res.send('This is destroy page');
}
