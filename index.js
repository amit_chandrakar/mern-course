import express from 'express';
import routes from './src/routes/index.js';
import { API_VERSION } from './src/utils/constants.js';
import 'dotenv/config';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.json());

app.use(API_VERSION, routes); // http://localhost:3000/api/v1/users

// connect to mongodb
mongoose.connect(process.env.MONGO_URL)
.then(() => {
    app.listen(process.env.PORT_NUMBER, () => {
        console.log('Server is listening on port 3000');
    });
})
.catch((err) => {
    console.log(err);
});


